﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmployeeManagement.Utilities
{
    public class ValidEmailDomainAttribute : ValidationAttribute
    {
        private readonly string[] allowedDomains;

        public ValidEmailDomainAttribute(string allowedDomains)
        {
            this.allowedDomains = allowedDomains.Split(';');
        }


        public override bool IsValid(object value)
        {
            string email = value.ToString();

            MailAddress address = new MailAddress(email);
            string host = address.Host;

            return allowedDomains.Contains(host);
        }
    }
}
