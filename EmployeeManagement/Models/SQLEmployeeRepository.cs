﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.Models
{
    public class SQLEmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext context;

        public SQLEmployeeRepository(AppDbContext context)
        {
            this.context = context;
        }
               

        public Employee Add(Employee emp)
        {
            context.Add(emp);
            context.SaveChanges();

            return emp;
        }

        public Employee Delete(int employeeId)
        {
            Employee empToDelete = context.Employees.Find(employeeId);
            if (empToDelete != null)
            {
                context.Employees.Remove(empToDelete);
                context.SaveChanges();
            }

            return empToDelete;
        }

        public IEnumerable<Employee> GetAllEmployee()
        {
            return context.Employees;
        }

        public Employee GetEmployee(int Id)
        {
            return context.Employees.Find(Id);
        }

        public Employee Update(Employee empChanges)
        {
            var employee = context.Employees.Attach(empChanges);
            employee.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();

            return empChanges;
        }
    }
}
