﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeManagement.Models
{
    public class MockEmployeeRepository : IEmployeeRepository
    {
        private List<Employee> _employees;

        public MockEmployeeRepository()
        {
            _employees = new List<Employee>()
            {
                new Employee() {Id=1, Name="Mary", Department=Dept.HR, Email="mary@rr.pt"},
                new Employee() {Id=2, Name="John", Department=Dept.IT, Email="john@rr.pt"},
                new Employee() {Id=3, Name="Miguel", Department=Dept.None, Email="mike@rr.pt"}

            };
            
        }

        public Employee Add(Employee emp)
        {
            int maxPlus1 = _employees.Max(p => p.Id) + 1;
            emp.Id = maxPlus1;
            _employees.Add(emp);
            return emp;
        }

        public Employee Delete(int employeeId)
        {
            Employee emp = _employees.FirstOrDefault(e => e.Id == employeeId);

            if (emp != null)
            {
                _employees.Remove(emp);
            }
            return emp;

        }

        public IEnumerable<Employee> GetAllEmployee()
        {
            return _employees;
        }

        public Employee GetEmployee(int Id)
        {
            return _employees.FirstOrDefault(e => e.Id == Id);
        }

        public Employee Update(Employee empChanges)
        {
            Employee emp = _employees.FirstOrDefault(e => e.Id == empChanges.Id);

            if (emp != null)
            {
                emp.Name = empChanges.Name;
                emp.Department = empChanges.Department;
                emp.Email = empChanges.Email;
            }
            return emp;

        }
    }
}
