﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Models;
using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using EmployeeManagement.Security;
using Microsoft.AspNetCore.DataProtection;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeManagement.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly ILogger logger;
        private readonly IDataProtector protector;


        public HomeController(IEmployeeRepository employeeRepository,
                                IHostingEnvironment hostingEnvironment,
                                ILogger<HomeController> logger,
                                IDataProtectionProvider dataProtectionProvider,
                                DataProtectionPurposeStrings dataProtectionPurposeStrings)
        {
            _employeeRepository = employeeRepository;
            this.hostingEnvironment = hostingEnvironment;
            this.logger = logger;
            this.protector = dataProtectionProvider.CreateProtector(dataProtectionPurposeStrings.EmployeeIdRouteValue);

        }

        // GET: /<controller>/
        [AllowAnonymous]
        public ViewResult Index()
        {
            var model = _employeeRepository.GetAllEmployee()
                    .Select(e =>
                    {
                        // Encrypt the ID value and store in EncryptedId property
                        e.EncryptedId = protector.Protect(e.Id.ToString());
                        return e;
                    });

            return View(model);
        }

        [AllowAnonymous]
        public ViewResult Details(string id)
        {
            // Throws an exception: testing purposes
            //throw new Exception("Just for fun: Error in details");

            logger.LogTrace("Trace Log");
            logger.LogDebug("Debug Log");
            logger.LogError("Error Log");

            // Decrypt the employee id using Unprotect method
            string decryptedId = protector.Unprotect(id);
            int decryptedIntId = Convert.ToInt32(decryptedId);

            Employee employee = _employeeRepository.GetEmployee(decryptedIntId);
            //Employee employee = _employeeRepository.GetEmployee(id.Value);

            if (employee == null)
            {
                Response.StatusCode = 404;
                return View("EmployeeNotFound", id);
            }

            HomeDetailsViewModel homeDetailsViewModel = new HomeDetailsViewModel()
            {
                Employee = employee,
                PageTitle = "Employee details"
            };

            return View(homeDetailsViewModel);
        }

        [HttpGet]
        //[Authorize]
        public ViewResult Create()
        {
            return View();
        }

        [HttpGet]
        //[Authorize]
        public ViewResult Edit(int id)
        {
            Employee emp = _employeeRepository.GetEmployee(id);
            EmployeeEditViewModel empEdit = new EmployeeEditViewModel
            {
                ID = emp.Id,
                Department = emp.Department,
                Email = emp.Email,
                Name = emp.Name,
                ExistingPhotoPath = emp.PhotoPath

            };
            return View(empEdit);
        }

        [HttpPost]
        //[Authorize]
        public IActionResult Edit(EmployeeEditViewModel emp)
        {
            if (ModelState.IsValid)
            {
                Employee employee = _employeeRepository.GetEmployee(emp.ID);

                employee.Name = emp.Name;
                employee.Department = emp.Department;
                employee.Email = emp.Email;
                employee.PhotoPath = emp.ExistingPhotoPath;

                if (emp.Photos != null)
                {
                    // Handle existing Photo
                    if (!string.IsNullOrEmpty(emp.ExistingPhotoPath))
                    {
                        string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                        string filePath = Path.Combine(uploadsFolder, emp.ExistingPhotoPath);
                        System.IO.File.Delete(filePath);

                    }
                    employee.PhotoPath = UpdatePhotoStorage(emp);
                }

                _employeeRepository.Update(employee);
                return RedirectToAction("index");
            }

            return View();
        }


        [HttpPost]
        //[Authorize]
        public IActionResult Create(EmployeeCreateViewModel emp)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = UpdatePhotoStorage(emp);

                Employee newEmp = new Employee
                {
                    Name = emp.Name,
                    Email = emp.Email,
                    Department = emp.Department,
                    PhotoPath = uniqueFileName
                };

                _employeeRepository.Add(newEmp);
                return RedirectToAction("details", new { id = newEmp.Id });
            }

            return View();
        }

        private string UpdatePhotoStorage(EmployeeCreateViewModel emp)
        {
            string uniqueFileName = null;
            if (emp.Photos != null)
            {
                if (emp.Photos.Count > 0)
                {

                    foreach (IFormFile photoFile in emp.Photos)
                    {
                        string uploadsFolder = Path.Combine(hostingEnvironment.WebRootPath, "images");
                        uniqueFileName = Guid.NewGuid().ToString() + "_" + photoFile.FileName;
                        string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            photoFile.CopyTo(fileStream);
                        }

                    }


                }
            }

            return uniqueFileName;
        }
    }
}
