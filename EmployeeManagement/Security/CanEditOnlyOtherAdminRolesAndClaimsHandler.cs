﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace EmployeeManagement.Security
{
    public class CanEditOnlyOtherAdminRolesAndClaimsHandler :
        AuthorizationHandler<ManageAdminRolesAndClaimsRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            ManageAdminRolesAndClaimsRequirement requirement)
        {
            if (!(context.Resource is AuthorizationFilterContext authFilterContext))
            {
                return Task.CompletedTask;
            }

            // get current admin user id
            string loggedInAdminId =
                context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            // retrieve from query string
            string adminIdBeingEdited = authFilterContext.HttpContext.Request.Query["userId"];

            if (context.User.IsInRole("Admin") &&
                context.User.HasClaim(claim => claim.Type == "Edit Role" && claim.Value == "true") &&
                adminIdBeingEdited.ToLower() != loggedInAdminId.ToLower())
            {
                // requirement is met!
                context.Succeed(requirement);
            }
            else
            {
                // This is an explicit failure. If this is ran, the test will fail independently of the sucess of others.
                // If failure is not returned explicitly, the requirement is met only if one of the handlers returns success.
                // If all handlers return "nothing" than the requirement fails.
                //context.Fail();         
            }

            return Task.CompletedTask;
        }
    }
}
